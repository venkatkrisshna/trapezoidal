! Trapezoidal Rulec
! Venkata Krisshna
! EMEC 592 - HPC & Multiphase Flows

! This program calculates an approximate value of an integral

program trapezoidal
  implicit none
  include 'mpif.h'
  integer, parameter :: wp=kind(1.0d0)

  integer ierror, rank, numproc, count, status, n, procs
  real(WP) local_int, total_int, local_a, local_b, local_n, h, a, b
  real(16), parameter :: PI_16 = 4 * atan (1.0_16)

  ! Integral Specs
  a = 0     ! lower bound
  b = 2*3.141597826    ! upper bound
  n = 10000    ! number of points

  call MPI_INIT(ierror)
  call MPI_COMM_RANK(mpi_comm_world,rank,ierror)
  call MPI_COMM_SIZE(mpi_comm_world,numproc,ierror)

  h       = (b-a)/n              ! Calculating h
  local_n = n/numproc            ! Number of points per processor
  local_a = a + rank*local_n*h   ! Lower bound for each processor
  local_b = local_a + local_n*h  ! Upper bound for each processor
  procs   = numproc-1

  ! Compute local trap sums
  call local_integral(local_a,local_b,local_n,h,local_int)

  ! Final summation
  if(rank.ne.0) then
     ! If rank!=0, send sum to proc 3
     call MPI_SEND(local_int,10,mpi_real,0,1,mpi_comm_world,ierror)
  else if(rank.eq.0)then
     ! Integration specs for printing
     print*,'Integrating function y = x^2'
     print*,'from',a,'to',b
     print*,'using',n,'points'
     print*,'and',numproc,'processors.'
     total_int = local_int
     count = 1
     do count = 1,procs
        ! If rank=0, receive from other procs
        call MPI_RECV(local_int,100,mpi_real,count,1,mpi_comm_world,status,ierror)
        total_int = total_int + local_int
     end do
     print *,'SOLUTION : Approximate integral is', total_int
  end if

  call MPI_FINALIZE(ierror)
end program trapezoidal

real function f(x)
  implicit none
  integer, parameter    :: wp=kind(1.0d0)
  real(WP), intent(in)  :: x
  ! Function : y = x^2
  f = x*x
  return
end function f

subroutine local_integral(local_a,local_b,local_n,h,local_int)
  integer, parameter    :: wp=kind(1.0d0)
  real(WP), intent(in)  :: local_a, local_b, local_n, h
  real(WP), intent(out) :: local_int
  real(WP) sum, x
  integer i
  x = local_a
  local_int = (f(local_a) + f(local_b))/2
  do i=1,local_n
     x   = x + h
     local_int = local_int + f(x)
  end do
  local_int = h*local_int
end subroutine local_integral
